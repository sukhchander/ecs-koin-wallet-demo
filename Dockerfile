FROM ubuntu
MAINTAINER sukhchander <sukhchander@gmail.com>

RUN apt-get update
RUN apt-get install -y dnsutils netcat-traditional
RUN update-alternatives --set nc /bin/nc.traditional
RUN apt-get install -y git wget
RUN apt-get install -y nodejs npm
RUN npm install -g http-server

RUN mkdir -p /home/www \
    && cd /home/www \
    && git clone https://sukhchander@bitbucket.org/sukhchander/koin-wallet-www.git

WORKDIR /home/www/koin-wallet-www

CMD ["http-server", "-s", "."]

EXPOSE 8080
