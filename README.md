## AWS ECS Demo


## Requirements

* [AWS CLI](http://docs.aws.amazon.com/cli)
* [Amazon EC2](https://aws.amazon.com/ec2)
* [Docker](https://www.docker.com/aws)
* [Amazon ECS](http://aws.amazon.com/ecs)
* [Amazon ECS Agent](https://github.com/aws/amazon-ecs-agent)


## Setup

~~~bash
git clone https://bitbucket.org/sukhchander/ecs-koin-wallet-demo.git
cd ecs-koin-wallet-demo
~~~

## AWS CLI

Ensure AWS CLI is set to use a region where Amazon ECS is available ie:
(`us-east-1`, `us-west-1`, `us-west-2`)

List AWS config:

~~~bash
aws configure list
~~~

Modify AWS config:

~~~bash
aws configure
~~~

## Setup and Configuration

~~~bash
./setup.sh
~~~

Output:

~~~bash
Creating ECS cluster (ecs-koin-wallet-cluster) ... ✔
Creating VPC (ecs-koin-wallet-vpc) ... ✔
Creating Subnet (ecs-koin-wallet-subnet) ... ✔
Creating Internet Gateway (ecs-koin-wallet) ... ✔
Creating Security Group (ecs-koin-wallet) ... ✔
Creating Key Pair (ecs-koin-wallet-key.pem) ... ✔
Creating IAM role (ecs-role) ... ✔
Creating Launch Configuration (ecs-launch-configuration) ... ✔
Creating Auto Scaling Group (ecs-koin-wallet-group) with 3 instances ... ✔
Waiting for instances to join the cluster (this may take a moment) ... ✔
Registering ECS Task Definition (ecs-koin-wallet-task) ... ✔
Creating ECS Service with 3 tasks (ecs-koin-wallet-service) ... ✔
Waiting for tasks to start running ... ✔
ECS cluster contains:
  http://ec2-52-43-128-222.us-west-2.compute.amazonaws.com
  http://ec2-52-38-42-122.us-west-2.compute.amazonaws.com
  http://ec2-35-162-29-155.us-west-2.compute.amazonaws.com
ECS cluster launched ... ✔
~~~

## Result

* Created an Amazon ECS cluster
* Spawned 3 EC2 instances that are now part of the ECS cluster
* Created an ECS task family that describes the HTTP Server and API containers


## Verify

Navigate to one of 3 instances (response is from HTTP Server container):

~~~bash
launchy http://ec2-35-162-29-155.us-west-2.compute.amazonaws.com
~~~
